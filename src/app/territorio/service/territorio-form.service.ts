import { Injectable } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';


@Injectable({
    providedIn: 'root'
})
export class TerritorioFormService {

    constructor(
        private formBuilder: FormBuilder
    ) {
    }
    getNewFormGroup() {
        let id = new FormControl("");
        let nome = new FormControl("", Validators.required);
        let codigo = new FormControl("", Validators.required);
        let fonteDado = new FormControl("", Validators.required);
        let sigla = new FormControl("");
        let categoria = new FormControl("", Validators.required);
        let territorioPai = new FormControl("");
        let zona = new FormControl("", Validators.required);
        let croqui = new FormControl("");
        let cep = new FormControl("", Validators.pattern(/^\d{8}$/));
        let poligono = new FormControl("",[ Validators.required]);
        let status = new FormControl("");
        let altitude = new FormControl("");
        

        let formGroup = this.formBuilder.group({
            "id": id,
            "nome": nome,
            "codigo": codigo,
            "fonteDado": fonteDado,
            "sigla": sigla,
            "categoria": categoria,
            "territorioPai": territorioPai,
            "zona": zona,
            "croqui": croqui,
            "cep": cep,
            "poligono": poligono,
            "status": status,
            "altitude": altitude,
        });
        return formGroup;
    }

}
