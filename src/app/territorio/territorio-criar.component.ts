import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';
import { FormGroup} from '@angular/forms';
import Territorio from '../model/territorio';
import { TerritorioFormService } from './service/territorio-form.service';
import { HttpTerritorioService } from './service/http-territorio.service';

/**
 * @title Table retrieving data through HTTP
 */
@Component({
    selector: 'app-territorio-criar',
    templateUrl: './territorio-criar.component.html',
    styleUrls: ['./territorio.component.scss']
})

export class TerritorioCriarComponent implements OnInit {

    formGroup: FormGroup;
    
    territorio: Territorio;

    constructor(
        private apiDataService: HttpTerritorioService,
        private router: Router,
        public snackBar: MatSnackBar,
        private dialog: MatDialog,     
        private territorioFormService: TerritorioFormService
        ) {
    }

    ngOnInit() {
        this.formGroup = this.territorioFormService.getNewFormGroup();
        this.territorio = new Territorio();  
    }

    salvar() {
        console.log("Salvar Territorio!", this.formGroup.value);

        const dialogoConfirmacao = this.dialog.open(DialogoConfirmacaoComponent, {
            data: { titulo: "Salvar", conteudo: "Confirma criação?" }
        });

        dialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                Territorio.popularBaseadoEmFormGroup(this.territorio, this.formGroup);
                this.apiDataService.salvar(this.territorio).pipe(
                    map((data: Territorio) => this.onSuccess(data)),
                    catchError(err => this.handleError(err))).subscribe();
            }
        });
    }

    onSuccess(data) {
        console.log("Tudo OK!", data)
        this.abrirSnackBar("Territorio cadastrado com sucesso!", null, "green-snackbar")
        this.router.navigate([`territorios`]);  
    }

    handleError(erro): any {
        console.log("Erro", erro)
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }

}
