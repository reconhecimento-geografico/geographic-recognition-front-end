
import  ListaDeTrabalho  from 'src/app/model/listaDeTrabalho';

import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { RelatorioService } from './relatorio.service';
import { WebDataRocksPivot } from '../webdatarocks/webdatarocks.angular4';
import { template } from '@angular/core/src/render3';
import * as localização from '../../BrazilianPortuguese.json';
import { ViewChild } from '@angular/core';
import Territorio from '../model/territorio';

@Component({
    selector: 'app-relatorio',
    templateUrl: './relatorio.component.html',
    styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit, AfterViewInit {
   
    @Input() listas : ListaDeTrabalho[];
    @Input() territorio : Territorio;
    @ViewChild('pivot1') child: WebDataRocksPivot;
    //guarda os dados que serao utilizados no relatorio
    dadosF = [];
    
    //flag utilizada para controlar se o componente será inicializado na tela ou nao
    iniciarComponente = false;
    constructor(public relatorioService: RelatorioService) {
        this.relatorioService = relatorioService;
        this.dadosF = this.relatorioService.dataReport;
    }

    onPivotReady(pivot: WebDataRocks.Pivot): void {
        console.log("[ready] WebDataRocksPivot", this.child);
    }
    removeTabs(event){
        console.log("para remover", event);
        var tabs = event.getTabs();
        event.getTabs = function() {
            let newTab = [tabs[3]];
            return newTab;
        }
    }

    onCustomizeCell(cell: WebDataRocks.CellBuilder, data: WebDataRocks.Cell): void {
        //console.log("[customizeCell] WebDataRocksPivot");
        if (data.isClassicTotalRow) cell.addClass("fm-total-classic-r");
        if (data.isGrandTotalRow) cell.addClass("fm-grand-total-r");
        if (data.isGrandTotalColumn) cell.addClass("fm-grand-total-c");
    }

    onReportComplete(): void {
        this.child.webDataRocks.off("reportcomplete");
        /* this.child.webDataRocks.setReport({
            dataSource: {
                filename: ""
            }
        }); */
    }
    ngOnInit() {
        //console.log("localização: " + JSON.stringify(localização.default));
        console.log("componente atual: ", this);
       
    }

    ngAfterViewInit(): void {
       
        if(this.listas)
        {
            this.relatorioService.getRelatoriosPorLista(this, this.listas);
            this.relatorioService.refreshReport(this);
        }

        if(this.territorio)
        {
            this.relatorioService.getRelatoriosPorTerritorio(this, this.territorio);
        } 
    }


    //retorna um JSON com a formatação dos dados no relatório e os dados que serão utilizados
    getReport() {

        return {
            dataSource: {
                //coloca os dados do relatorio dentro do JSON
                data: this.dadosF
            },
            //formatação do relatório(colunas, linhas, filtros...) 
            "slice": {
                "rows": [
                    {
                        "uniqueName": "territorio Pai"
                    },
                    {
                        "uniqueName": "territorio"
                    },
                    {
                        "uniqueName": "imovel"
                    },
                    {
                        "uniqueName": "tipo"
                    }
                ],
                "columns": [
                    {
                        "uniqueName": "Measures"
                    }
                ],
                "measures": [
                    {
                        "uniqueName": "tipo",
                        "aggregation": "count",
                        "availableAggregations": [
                            "count",
                            "distinctcount"
                        ]
                    },
                    {
                        "uniqueName": "Quantidade de Habitantes",
                        "aggregation": "sum"
                    },
                    {
                        "uniqueName": "Outros depositos desprotegidos",
                        "aggregation": "sum"
                    },
                    {
                        "uniqueName": "Numero de Caixas Dagua desprotegidas",
                        "aggregation": "sum"
                    },
                    {
                        "uniqueName": "Quantidade de Familias",
                        "aggregation": "sum"
                    },
                    {
                        "uniqueName": "Numero de Armadilhas instaladas",
                        "aggregation": "sum"
                    }
                ],
                "expands": {
                    "expandAll" : true
                }
            },
            "options": {
                "grid": {
                    "type": "classic"
                },
                "showAggregationLabels": false
            },
            "localization": localização.default
        }
    }



}
