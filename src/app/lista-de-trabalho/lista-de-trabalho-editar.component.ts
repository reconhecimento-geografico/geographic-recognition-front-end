import { HttpListaDeTrabalhoService } from './service/http-lista-de-trabalho.service';

import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogoConfirmacaoComponent } from '../dialogo-confirmacao/dialogo-confirmacao.component';
import Imovel from '../model/imovel';
import { FormGroup } from '@angular/forms';
import { delay } from 'rxjs/operators';
import ListaDeTrabalho from '../model/listaDeTrabalho';
import { ListaDeTrabalhoFormService } from './service/lista-de-trabalho-form.service';
/**
 * @title Table retrieving data through HTTP
 */
@Component({
    selector: 'app-lista-de-trabalho-editar',
    templateUrl: './lista-de-trabalho-editar.component.html',
    styleUrls: ['./lista-de-trabalho.component.scss']
})

export class ListaDeTrabalhoEditarComponent implements OnInit {

    formGroup: FormGroup;
    
    lista: ListaDeTrabalho = new ListaDeTrabalho();
    id: string;
    constructor(
    
        private activatedRoute: ActivatedRoute,
        private router: Router,
        public matSnackBar: MatSnackBar,
        private matDialog: MatDialog,
        private apiListaService: HttpListaDeTrabalhoService,
        private listaFormService : ListaDeTrabalhoFormService
    ) {
        this.formGroup = this.listaFormService.getNewFormGroup();
        this.activatedRoute.params.subscribe(params => this.id = params.id);
        
    }

    ngOnInit() {
        this.pesquisarPorId();
    }
    pesquisarPorId() {
        this.apiListaService.pesquisarPorId(this.id).pipe( delay(0),
            map((data: ListaDeTrabalho) => this.tratarSucessoAoPesquisarPorId(data)),
            catchError(err => this.tratarErroAoPesquisarPorId(err))).subscribe();
    }
    atualizar() {
        const dialogoConfirmacao = this.matDialog.open(DialogoConfirmacaoComponent, {
            data: {titulo: "Alterar", conteudo: "Confirma alterações?"}
        });

        dialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                ListaDeTrabalho.popularBaseadoEmFormGroup(this.lista, this.formGroup);
                console.log('Atualizar-> ',this.lista);
                this.apiListaService.atualizarSemId(this.lista).pipe(
                    map((data: ListaDeTrabalho) => this.tratarSucessoAoAtualizar(data)),
                    catchError(err => this.tratarErroAoAtualizar(err))).subscribe();
            }
        });
        
    }

    tratarSucessoAoPesquisarPorId(data: ListaDeTrabalho) {
        this.lista = data;
        this.formGroup.patchValue(this.lista);
    }
    tratarErroAoPesquisarPorId(erro): any {
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }

    tratarSucessoAoAtualizar(data: ListaDeTrabalho) {
        this.abrirSnackBar("Registro atualizado com sucesso!",null, "green-snackbar")        
        this.router.navigate([`listas-de-trabalho`]); 
    }

    tratarErroAoAtualizar(erro): any {
        console.log(erro.error);
        this.abrirSnackBar("Ops... "+erro.error,null, "red-snackbar") 
        return [];
    }
    
    

    excluir() {
        const matDialogoConfirmacao = this.matDialog.open(DialogoConfirmacaoComponent, {
            data: { titulo: "Confirmação", conteudo: "Confirma exclusão do registro?" }
        });

        matDialogoConfirmacao.afterClosed().subscribe(result => {
            if (result) {
                this.apiListaService.deletar(this.lista).pipe(
                    map((data: ListaDeTrabalho) => this.tratarSucessoAoDeletar(data)),
                    catchError(err => this.tratarErroAoDeletar(err))).subscribe();
            }
        });

    }

    tratarSucessoAoDeletar(data) {
        this.abrirSnackBar("Registro deletado com sucesso!", null, "green-snackbar")
        this.router.navigate([`listas-de-trabalho`]);
    }

    tratarErroAoDeletar(erro): any {
        let mensagem = "";
        if(erro.message != null && erro.message.includes("ConstraintViolationException")){
            mensagem = "Não foi possível deletar. Registro é pai de outros"
        } else {
            mensagem = erro;
        }

        if(erro != null && erro.includes("ConstraintViolationException")){
            mensagem = "Não foi possível deletar. Registro possui relacionamento com outras entidades."
        }

        this.abrirSnackBar("Erro ao deletar: " + mensagem, null, "red-snackbar")
        return [];
    }

    abrirSnackBar(message: string, action: string, classe: string) {
        this.matSnackBar.open(message, action, {
            duration: 3000,
            panelClass: [classe]
        });
    }

}
