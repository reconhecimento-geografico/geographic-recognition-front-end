import  Categoria  from 'src/app/model/categoria';

import { FormGroup } from '@angular/forms';
import FonteDado from './fonteDado';

export default class Territorio{
    id : string;
    codigo  : string;
    fonteDado  : FonteDado;
    sigla  : string;
    nome  : string;
    categoria : Categoria;
    territorioPai : Territorio;
    zona : string;
    croqui : string;
    cep : string;
    poligono : string[][];
    status : string;
    altitude : string;
    filhos : any[];

    public static popularBaseadoEmFormGroup(registro: Territorio, formGroup: FormGroup) {
        registro.nome =  formGroup.value.nome;
        registro.codigo =  formGroup.value.codigo;
        registro.fonteDado =  formGroup.value.fonteDado;
        registro.sigla =  formGroup.value.sigla;
        registro.categoria =  formGroup.value.categoria;
        registro.territorioPai =  formGroup.value.territorioPai ? formGroup.value.territorioPai : null;
        registro.zona =  formGroup.value.zona;
        registro.croqui =  formGroup.value.croqui;
        registro.cep =  formGroup.value.cep;
        registro.poligono =  JSON.parse(formGroup.value.poligono);
        registro.status =  formGroup.value.status;
        registro.altitude =  formGroup.value.altitude;
        
    }
}
