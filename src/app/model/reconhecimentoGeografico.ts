import { FormGroup } from '@angular/forms';
import { Formulario } from './Formulario';

export default class ReconhecimentoGeografico{
    id : string;
    titulo  : string;
    conteudo : any;
    formulario : Formulario;
   
    constructor(titulo : string, conteudo : any, formulario : Formulario){
        this.titulo = titulo;
        this.conteudo = conteudo;
        this.formulario = formulario;
    }

    public static popularBaseadoEmFormGroup(registro: ReconhecimentoGeografico, formGroup: FormGroup) {
        registro.titulo =  formGroup.value.titulo;
        registro.conteudo =  formGroup.value.conteudo;
        registro.formulario =  formGroup.value.formulario;
       
        
    }
}
