import { FormGroup } from '@angular/forms';
import ItemLista from './itemLista';


export default class ListaDeTrabalho{
    id : number;
    nome  : string;
    descricao  : string;
    itens : ItemLista[];
   

    public static popularBaseadoEmFormGroup(registro: ListaDeTrabalho, formGroup: FormGroup) {
        
        registro.nome =  formGroup.value.nome;
        registro.descricao =  formGroup.value.descricao;
        registro.itens =  formGroup.value.itens;
        
    }
}
