import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpGenerico } from 'src/app/service-generico/http.service';
import Categoria from 'src/app/model/Categoria';
@Injectable({
    providedIn: 'root'
})
export class HttpCategoriaService extends HttpGenerico<Categoria> {

    constructor(http: HttpClient) {
        super(http, '/categoria')
    }

  


}
