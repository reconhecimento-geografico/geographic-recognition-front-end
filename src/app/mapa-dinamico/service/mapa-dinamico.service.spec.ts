import { TestBed } from '@angular/core/testing';

import { MapaDinamicoService } from './mapa-dinamico.service';

describe('MapaDinamicoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MapaDinamicoService = TestBed.get(MapaDinamicoService);
    expect(service).toBeTruthy();
  });
});
