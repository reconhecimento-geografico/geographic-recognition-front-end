
import { HttpCategoriaService } from '../service-generico/http-categoria.service';
import { Map } from 'ol/Map.js';
import { Component, ChangeDetectionStrategy, OnInit, ViewChild, ElementRef, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize, delay, map, catchError } from 'rxjs/operators';
import Categoria from '../model/categoria';
import Imovel from '../model/imovel';
import { MatSnackBar } from '@angular/material';
import TipoImovel from '../model/tipoImovel';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpTipoImovelService } from '../service-generico/http-tipo-imovel.service';
import Territorio from '../model/territorio';
import { ImovelFormService } from './service/imovel-form.service';
import { HttpTerritorioService } from '../territorio/service/http-territorio.service';
import { HttpImovelService } from './service/http-imovel.service';
@Component({
  selector: 'app-imovel-form',
  templateUrl: './imovel-form.component.html',
  styleUrls: ['./imovel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImovelFormComponent implements OnInit {
  @ViewChild("mapa") mapa;
  @Input() formGroup: FormGroup;
  @Input() criando: boolean;
  @Output() excluir = new EventEmitter();
  imoveisIrmaos: Observable<Imovel[]>;
  imoveisIrmaosSubject: BehaviorSubject<Imovel[]>;

  territorio: Observable<Territorio[]>;
  territorioSubject: BehaviorSubject<Territorio[]>;

 
  tipoImoveisFiltrados: TipoImovel[] = [];
  isLoadingTipoImovel: boolean = false;
  tipoImovelSelecionado: TipoImovel;

  resultadoPesquisaTerritorio: Territorio[] = [];
  carregandoPesquisaTerritorio: boolean = false;
  territorioSelecionada: Territorio;


  constructor(private imovelFormService: ImovelFormService,
    private httpImovelService: HttpImovelService,
    private httpTipoImovelService: HttpTipoImovelService,
    private apiTerritorioService: HttpTerritorioService,
    public matSnackBar: MatSnackBar,
  ) {
    this.imoveisIrmaosSubject = new BehaviorSubject<Imovel[]>(null);
    this.imoveisIrmaos = this.imoveisIrmaosSubject.asObservable();
    this.territorioSubject = new BehaviorSubject<Territorio[]>(null);
    this.territorio = this.territorioSubject.asObservable();
  }

  ngOnInit() {
    

    this.configurarDropdownTerritorio();
    this.configurarDropdownTipoImovel();

    if (this.formGroup.controls['territorio'].value) {
      this.pesquisarIrmaos( this.formGroup.controls['id'].value,
       this.formGroup.controls['territorio'].value);
      this.territorioSubject.next([this.formGroup.controls['territorio'].value]);
     
    }
  }

  ngAfterViewInit(): void {
    console.log('mapa: ', this.mapa.map.updateSize());
  }
  criarPoligono() {
    this.mapa.adicionarInteracaoDesenharPoligono();
  }

  tratarPoligonoCriado(event: any) {
    console.log(event.getCoordinates());
    let coords = event.getCoordinates();
    let coordsString = JSON.stringify(coords[0]);

    console.log('poligono criado', coordsString);
    this.formGroup.controls['poligono'].setValue(coordsString);

  }


 
  configurarDropdownTipoImovel() {
    this.formGroup
      .get('tipoImovel')
      .valueChanges
      .pipe(
        debounceTime(300),
        tap(() => this.isLoadingTipoImovel = true),
        switchMap(value => this.httpTipoImovelService.pesquisarPorNome(value)
          .pipe(
            finalize(() => this.isLoadingTipoImovel = false),
          )
        )
      )
      .subscribe(categorias => {
        this.tipoImoveisFiltrados = categorias;
      });

    this.formGroup.get("tipoImovel").setValue(this.formGroup.value.tipoImovel);
  }

  selecionarTipoImovel(event: any) {
    this.tipoImovelSelecionado = event.option.value;
  }

  checarSelecaoTipoImovel() {
    if (!this.tipoImovelSelecionado || this.tipoImovelSelecionado !== this.formGroup.controls['tipoImovel'].value) {
      this.formGroup.controls['tipoImovel'].setValue(null);
      this.tipoImovelSelecionado = null;
    }
  }

  displayFnTipoImovel(tipoImovel: TipoImovel) {
    if (tipoImovel) { return tipoImovel.nome; }
  }

  configurarDropdownTerritorio() {
    this.formGroup
      .get('territorio')
      .valueChanges
      .pipe(
        debounceTime(300),
        tap(() => this.carregandoPesquisaTerritorio = true),
        switchMap(value => this.apiTerritorioService.pesquisarPorNome(value)
          .pipe(
            finalize(() => this.carregandoPesquisaTerritorio = false),
          )
        )
      )
      .subscribe(territorios => {
        this.resultadoPesquisaTerritorio = territorios;
      });

    this.formGroup.get("territorio").setValue(this.formGroup.value.territorio);

  }
  selecionarTerritorio(event: any) {
    this.territorioSelecionada = event.option.value;
    this.pesquisarIrmaos(this.formGroup.controls['id'].value, this.territorioSelecionada);
    this.territorioSubject.next([this.territorioSelecionada]);

  }

  checarSelecaoTerritorio() {
    if (!this.territorioSelecionada || this.territorioSelecionada !== this.formGroup.controls['territorio'].value) {
      this.formGroup.controls['territorio'].setValue(null);
      this.territorioSelecionada = null;
    }
  }

  formatarExibicaoTerritorio(territorio: Territorio) {
    if (territorio) { return territorio.territorioPai ? 
      territorio.nome + ', '+  territorio.territorioPai.nome :
      territorio.nome
    ; }
  }
  pesquisarIrmaos( id : string, territorio: Territorio) {
    this.httpImovelService.pesquisarPorTerritorio(id,territorio).pipe(delay(0),
      map((data: Imovel[]) => this.tratarSucessoAoPesquisarPorTerritorio(data)),
      catchError(err => this.tratarErroAoPesquisarPorTerritorio(err))).subscribe();
  }
  tratarSucessoAoPesquisarPorTerritorio(data: Imovel[]) {

    this.imoveisIrmaosSubject.next(data);
    console.log('irmãos: ', this.imoveisIrmaos);


  }
  tratarErroAoPesquisarPorTerritorio(erro): any {
    this.abrirSnackBar("Ops... " + erro.error, null, "red-snackbar")
    return [];
  }
  abrirSnackBar(message: string, action: string, classe: string) {
    this.matSnackBar.open(message, action, {
      duration: 3000,
      panelClass: [classe]
    });
  }
}
