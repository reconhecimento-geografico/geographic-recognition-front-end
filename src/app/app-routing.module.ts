import { ListaDeTrabalhoEditarComponent } from './lista-de-trabalho/lista-de-trabalho-editar.component';
import { ListaDeTrabalhoListarComponent } from './lista-de-trabalho/lista-de-trabalho-listar.component';
import { ListaDeTrabalhoCriarComponent } from './lista-de-trabalho/lista-de-trabalho-criar.component';
import { TerritorioEditarComponent } from './territorio/territorio-editar.component';
import { TerritorioListarComponent } from './territorio/territorio-listar.component';
import { TerritorioFormComponent } from './territorio/territorio-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TerritorioCriarComponent } from './territorio/territorio-criar.component';
import { ImovelCriarComponent } from './imovel/imovel-criar.component';
import { ImovelListarComponent } from './imovel/imovel-listar.component';
import { ImovelEditarComponent } from './imovel/imovel-editar.component';
import { ConsolidacaoPorListaListarComponent } from './consolidacao/consolidacao-por-lista-listar.component';
import { ConsolidacaoPorTerritorioListarComponent } from './consolidacao/consolidacao-por-territorio-listar.component';
import { RelatorioComponent } from './consolidacao/relatorio.component';
import { MonitoramentoComponent } from './monitoramento/monitoramento.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/territorios',
    pathMatch: 'full'
  },
  {
    path : "territorio-criar",
    component : TerritorioCriarComponent
  },
  {
    path : "territorios",
    component : TerritorioListarComponent
  },
  {
    path : "territorio-editar/:id",
    component : TerritorioEditarComponent
  },
  {
    path : "imovel-criar",
    component : ImovelCriarComponent
  },
  {
    path : "imoveis",
    component : ImovelListarComponent
  },
  {
    path : "imovel-editar/:id",
    component : ImovelEditarComponent
  },
  {
    path : "lista-de-trabalho-criar",
    component : ListaDeTrabalhoCriarComponent
  },
  {
    path : "listas-de-trabalho",
    component : ListaDeTrabalhoListarComponent
  },
  {
    path : "lista-de-trabalho-editar/:id",
    component : ListaDeTrabalhoEditarComponent
  },
  {
    path : "consolidacao-por-lista-listar",
    component : ConsolidacaoPorListaListarComponent
  },
  {
    path : "consolidacao-por-territorio-listar",
    component : ConsolidacaoPorTerritorioListarComponent
  },
  {
    path : "monitoramento",
    component : MonitoramentoComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
